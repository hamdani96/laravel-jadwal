@extends('layouts.app')

@section('title', 'Beranda')

@section('content')
<div class="jumbotron"></div>

<div class="row">
    <div class="">
        <div class="">
            <div class="col-md-12">
                @foreach($datas as $r)
                <div class="col-md-12">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8 col-sm-12">
                                    <div class="perawat-card">
                                        <img src="{{ asset('assets_admin/images/perawat/perawat.jpg') }}" class="perawat-img" alt="">
                                        <ul>
                                            <li><h3>{{ $r->user->name }}</h3></li>
                                            <li><span>Perawat</span></li>
                                            <li><font style="color: orange">{{ date('d/m/y', strtotime($r->started_at)) }}</font> - {{ date('d/m/y', strtotime($r->ended_at)) }}</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card-status">
                                    <h2>Status</h2>
                                    <!-- <div class="work-status"> Sedang Bertugas</div> -->
                                    @if(strtotime($r->started_at) <= time() && time() >= strtotime($r->ended_at))
                                    <button class="btn btn-success"> Selesai</button>
                                    @else
                                    <button class="btn btn-warning"> Sedang Bertugas</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                {{ $datas->links() }}
            </div>
        </div>
    </div>
</div>
@endsection