<!DOCTYPE html>
<html lang="en">

<head>
    <title>Pendekar Koding | @yield('title')</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
    	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    	<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Phoenixcoded" />
    <!-- Favicon icon -->
    <link rel="icon" href="{{ asset('assets_admin/images/favicon.ico') }}" type="image/x-icon">

    <!-- vendor css -->
    <link rel="stylesheet" href="{{ asset('assets_admin/css/style.css') }}">
    @yield('plus_css')
    
    

</head>
<body class="">
	<!-- [ Pre-loader ] start -->
	<div class="loader-bg">
		<div class="loader-track">
			<div class="loader-fill"></div>
		</div>
	</div>
	<!-- [ Pre-loader ] End -->
    <!-- [ navigation menu ] start -->
    @include('layouts.admins.sidebar')
    <!-- [ navigation menu ] end -->
    <!-- [ Header ] start -->
    @include('layouts.admins.header')
    <!-- [ Header ] end -->
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            {{-- Content --}}
            @yield('content')
            {{-- End Content --}}
        </div>
    </div>
    <!-- [ Main Content ] end -->
    
<!-- Required Js -->
<script src="{{ asset('assets_admin/js/vendor-all.min.js') }}"></script>
<script src="{{ asset('assets_admin/js/plugins/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets_admin/js/ripple.js') }}"></script>
<script src="{{ asset('assets_admin/js/pcoded.min.js') }}"></script>

<!-- Apex Chart -->
<script src="{{ asset('assets_admin/js/plugins/apexcharts.min.js') }}"></script>


<!-- custom-chart js -->
<script src="{{ asset('assets_admin/js/pages/dashboard-main.js') }}"></script>

@yield('plus_js')
</body>

