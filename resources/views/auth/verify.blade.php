<!DOCTYPE html>
<html lang="en">

<head>

	<title> Pendekar Koding | Sign</title>
	<!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 11]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="" />
	<meta name="keywords" content="">
	<meta name="author" content="Phoenixcoded" />
	<!-- Favicon icon -->
	<link rel="icon" href="{{ asset('assets_admin/images/favicon.ico') }}" type="image/x-icon">

	<!-- vendor css -->
	<link rel="stylesheet" href="{{ asset('assets_admin/css/style.css') }}">
	
	


</head>
<body>
    
    <div class="container" style="margin-top: 15rem">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Verifikasi Email Kamu </div>
    
                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('A fresh verification link has been sent to your email address.') }}
                            </div>
                        @endif
    
                        
                        Sebelum melanjutkan, periksa email Anda untuk tautan verifikasi. Jika Anda tidak menerima email,
                        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <button type="submit" class="btn btn-link p-0 m-0 align-baseline">
                                klik di sini untuk mengirim ulang verifikasi</button>.
                        </form>
                        
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-secondary" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Required Js -->
<script src="{{ asset('assets_admin/js/vendor-all.min.js') }}"></script>
<script src="{{ asset('assets_admin/js/plugins/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets_admin/js/ripple.js') }}"></script>
<script src="{{ asset('assets_admin/js/pcoded.min.js') }}"></script>



</body>

</html>