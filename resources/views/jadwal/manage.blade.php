@extends('layouts.admins.app')
@section('title', 'Data Jadwal')
@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Jadwal</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#!">Jadwal</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('add_jadwal') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Jadwal</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nama Perawat</th>
                                <th>Periode</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($jadwals as $jadwal)
                                <tr>
                                    <td>{{ $jadwal->id }}</td>
                                    <td>{{ $jadwal->user->name }}</td>
                                    <td>{{ date('d/m/y', strtotime($jadwal->started_at)) }} - {{ date('d/m/y', strtotime($jadwal->ended_at)) }}</td>
                                    <td>
                                        @if(strtotime($jadwal->started_at) <= time() && time() >= strtotime($jadwal->ended_at))
                                        <span class="badge badge-success">Selesai</span>
                                        @else
                                        <span class="badge badge-info">Sedang Bertugas</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection