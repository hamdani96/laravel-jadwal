@extends('layouts.admins.app')
@section('title', 'Add')
@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Add Jadwal</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin') }}"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#!">Jadwal</a></li>
                    <li class="breadcrumb-item"><a href="#!">Add Jadwal</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header"></div>
            <form action="{{ route('store_jadwal') }}" method="post">
            @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for=""> Nama Perawat</label>
                            <input type="text" name="none" class="form-control" readonly="" value="{{ Auth::user()->name }}" id="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for=""> Tanggal Mulai</label>
                            <input type="date" name="started_at" id="" class="form-control" value="{{ old('started_at') }}" required>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('started_at') }}</strong>
                            </span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Tanggal Berakhir</label>
                            <input type="date" name="ended_at" class="form-control" value="{{ old('ended_at') }}" required id="">
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ended_at') }}</strong>
                            </span>
                        </div>
                        <!-- Hidden Input -->
                        <input type="hidden" name="perawat_id" value="{{ Auth::user()->id }}">
                        <!-- End -->
                        <div class="form-group col-md-12">
                            <input type="submit" value="Simpan" class="btn btn-primary">
                            <a href="{{ route('jadwal') }}" class="btn btn-secondary">Batal</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection