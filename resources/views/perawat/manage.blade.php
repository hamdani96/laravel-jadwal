@extends('layouts.admins.app')
@section('title', 'Data Perawat')
@section('content')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Bootstrap Basic Tables</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#!">Bootstrap Table</a></li>
                    <li class="breadcrumb-item"><a href="#!">Basic Tables</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
                <a href="" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Perawat</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datas as $perawat)
                                <tr>
                                    <td>{{ $perawat->id }}</td>
                                    <td>{{ $perawat->name }}</td>
                                    <td>{{ $perawat->email }}</td>
                                    <td>
                                        @if ($perawat->email_verified_at != NULL)
                                        <span class="badge badge-success"> Aktif</span>
                                        @else 
                                        <span class="badge badge-danger"> Tidak Aktif</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a> | 
                                        <a href="#" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection