<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index')->name('admin');

Route::get('/perawat', 'PerawatController@index')->name('perawat');
Route::get('/jadwal', 'PerawatController@jadwal')->name('jadwal');
Route::get('/jadwal/add', function(){
    return view('jadwal/add');
})->name('add_jadwal');
Route::post('/jadwal/store', 'PerawatController@store_jadwal')->name('store_jadwal');