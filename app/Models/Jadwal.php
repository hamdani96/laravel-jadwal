<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $fillable = ['perawat_id', 'started_at', 'ended_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'perawat_id', 'id');
    }
}
