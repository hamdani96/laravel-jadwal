<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Jadwal;
use Auth;
use Carbo\Carbon;

class PerawatController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        $datas = User::where('level', 'perawat')->orderBy('id', 'desc')->get();
        return view('perawat/manage', compact('datas'));
    }

    public function jadwal()
    {
        if(Auth::user()->level == 'admin'){
            $jadwals = Jadwal::orderBy('id', 'desc')->get();
        }elseif(Auth::user()->level == 'perawat'){
            $jadwals = Jadwal::where('perawat_id', Auth::user()->id)->orderBy('id', 'desc')->get();
        }

        // $today_datetime = Carbon::now();
        // $get = Jadwal::where('started_at', '<=', $today_datetime)->where('ended_at', '>=', $today_datetime);
        return view('jadwal/manage', compact('jadwals'));
    }

    public function store_jadwal(Request $request)
    {
        $this->validate($request, [
            'started_at' => 'required',
            'ended_at' => 'required',
            'perawat_id' => 'required'
        ]);

        Jadwal::create([
            'started_at' => $request->started_at,
            'ended_at' => $request->ended_at,
            'perawat_id' => $request->perawat_id
        ]);

        return redirect()->route('jadwal');
    }
}
