<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jadwal;
use App\Models\User;

class FrontController extends Controller
{
    public function index()
    {
        $datas = Jadwal::orderBy('id', 'desc')->paginate(6);
        return view('front/index', compact('datas'));
    }
}
